const returnCallDataErr=(data,err,done,error)=>{
  if(err) error(data,err)
   else done(data);  
}
function compressionTool(){
  this.options={};
  return this;
}
compressionTool.prototype.set=function(driver,name=driver){
  try{
    if(!this.compressFunction.hasOwnProperty(name)) throw Error(name+" does not exist")
    this[name]=require(driver);
    this.compress=this.compressFunction[name].bind(this);
    this.decompress=this.decompressFunction[name].bind(this);
  } catch(ex) {
    console.error("compressionTool set error: "+ex.message)
    this.compress=this.noDriverError;
    this.decompress=this.noDriverError;
  }
  return this;
}
compressionTool.prototype.setBrotli=function(call){this.set('node:zlib','brotli');}
compressionTool.prototype.setFlate=function(call){this.set('node:zlib','flate');}
compressionTool.prototype.setGzip=function(call){this.set('node:zlib','gzip');}
compressionTool.prototype.setGzipSpeed=function(call){this.set('node:zlib','gzipSpeed');}
compressionTool.prototype.setGzipCompression=function(call){this.set('node:zlib','gzipCompression');}
compressionTool.prototype.setLzma=function(call){this.set('lzma');}
compressionTool.prototype.setLzmaCompression=function(call){this.set('lzma','lzmaCompression');}
compressionTool.prototype.setSnappy=function(call){this.set('snappy');}
compressionTool.prototype.setZlib=function(call){this.set('node:zlib','zlib');}
compressionTool.prototype.setZlibSpeed=function(call){this.set('node:zlib','zlibSpeed');}
compressionTool.prototype.setZlibCompression=function(call){this.set('node:zlib','zlibCompression');}
compressionTool.prototype.compress=function(data,done,error){
  this.setGzip();
  this.compress(data,done,error);
  return this;
}
compressionTool.prototype.decompress=function(data,done,error){
  this.setGzip();
  this.decompress(data,done,error);
  return this;
}
compressionTool.prototype.decompressDynamic=function(data,done,error){
  if(!this.decompressStack==null){
    this.decompressStack.push((new compressionTool()).setZlib())
  }
  this.setGzip();
  this.decompress(data,done,error);
  return this;
}
compressionTool.prototype.compressFunction={
  brotli:function(data,done,error){
    this.brotli.brotliCompress(data,(err,compressed)=>returnCallDataErr(compressed,err,done,error))
    return this;
  },
  flate:function(data,done,error){
    this.flate.deflate(
      data,
      (err, data)=>returnCallDataErr(data,err,done,error)
    );
    return this;
  },
  gzip:function(data,done,error){
    this.gzip.gzip(data,{level:this.gzip.constants.Z_DEFAULT_COMPRESSION},(err, compressed)=>returnCallDataErr(compressed,err,done,error))
    return this;
  },
  gzipSpeed:function(data,done,error){
    this.gzipSpeed.gzip(data,{level:this.gzipSpeed.constants.Z_BEST_SPEED},(err, compressed)=>returnCallDataErr(compressed,err,done,error))
    return this;
  },
  gzipCompression:function(data,done,error){
    this.gzipCompression.gzip(data,{level:this.gzipCompression.constants.Z_BEST_COMPRESSION},(err, compressed)=>returnCallDataErr(compressed,err,done,error))
    return this;
  },
  lzma:function(data,done,error){
    this.lzma.compress(data,1,(compressed,err)=>returnCallDataErr(compressed,err,done,error))
    return this;
  },
  lzmaCompression:function(data,done,error){
    this.lzmaCompression.compress(data,9,(compressed,err)=>returnCallDataErr(compressed,err,done,error))
    return this;
  },
  snappy:function(data,done,error){
    try{
      this.snappy.compress(data,(err,compressed)=>returnCallDataErr(compressed,err,done,error));
    } catch(ex) {
      error(ex.message)
    }
    return this;
  },
  zlib:function(data,done,error){
    this.zlib.gzip(data,{level:this.zlib.constants.Z_DEFAULT_COMPRESSION},(err, compressed)=>returnCallDataErr(compressed,err,done,error))
    return this;
  },
  zlibSpeed:function(data,done,error){
    this.zlibSpeed.gzip(data,{level:this.zlibSpeed.constants.Z_BEST_SPEED},(err, compressed)=>returnCallDataErr(compressed,err,done,error))
    return this;
  },
  zlibCompression:function(data,done,error){
    this.zlibCompression.gzip(data,{level:this.zlibCompression.constants.Z_BEST_COMPRESSION},(err, compressed)=>returnCallDataErr(compressed,err,done,error))
    return this;
  }
};
compressionTool.prototype.decompressFunction={
  brotli:function(compressed,done,error){
    this.brotli.brotliDecompress(compressed,(err,data)=>returnCallDataErr(data,err,done,error));
    return this;
  },
  flate:function(compressed,done,error){
    this.flate.inflate(
      compressed,
      {finishFlush: this.flate.constants.Z_FULL_FLUSH },
      (err, data)=>returnCallDataErr(data,err,done,error)
    );
    return this;
  },
  gzip:function(compressed,done,error){
    this.gzip.gunzip(compressed,
      {finishFlush: this.gzip.constants.Z_FULL_FLUSH },
      (err,data)=>returnCallDataErr(data,err,done,error)
    );
    return this;
  },
  gzipSpeed:function(compressed,done,error){
    this.gzipSpeed.gunzip(compressed,
      {finishFlush: this.gzipSpeed.constants.Z_FULL_FLUSH },
      (err,data)=>returnCallDataErr(data,err,done,error)
    );
    return this;
  },
  gzipCompression:function(compressed,done,error){
    this.gzipCompression.gunzip(compressed,
      {finishFlush: this.gzipCompression.constants.Z_FULL_FLUSH },
      (err,data)=>returnCallDataErr(data,err,done,error)
    );
    return this;
  },
  lzma:function(compressed,done,error){
    this.lzma.decompress(compressed,
      (data,err)=>returnCallDataErr(data,err,done,error)
//      ,(percent)=>{}
    );
    return this;
  },
  lzmaCompression:function(compressed,done,error){
    this.lzmaCompression.decompress(compressed,
      (data,err)=>returnCallDataErr(data,err,done,error)
//      ,(percent)=>{}
    );
    return this;
  },
  snappy:function(compressed,done,error){
    try{
      this.snappy.uncompress(compressed,{asBuffer:false},(err,data)=>returnCallDataErr(data,err,done,error));
    } catch(ex) {
      error(ex.message)
    }
    return this;
  },
  zlib:function(compressed,done,error){
    this.zlib.unzip(compressed,
      {finishFlush: this.zlib.constants.Z_FULL_FLUSH },
      (err,data)=>returnCallDataErr(data,err,done,error)
    );
    return this;
  },
  zlibSpeed:function(compressed,done,error){
    this.zlibSpeed.unzip(compressed,
      {finishFlush: this.zlibSpeed.constants.Z_FULL_FLUSH },
      (err,data)=>returnCallDataErr(data,err,done,error)
    );
    return this;
  },
  zlibCompression:function(compressed,done,error){
    this.zlibCompression.unzip(compressed,
      {finishFlush: this.zlibCompression.constants.Z_FULL_FLUSH },
      (err,data)=>returnCallDataErr(data,err,done,error)
    );
    return this;
  },
};
compressionTool.prototype.signatureScan=(data)=>{
  const signature=compressionTool.prototype.signature;
  const dataLength=data.length
  return Object.keys(compressionTool.prototype.signature)
    .filter(c=>{
      const s=signature[c],sl=s.length;
      return dataLength>=sl && s.compare(data,0,sl,0,sl)==0
    });
}
compressionTool.prototype.signature={
  "7z":Buffer.alloc(6,'377abcaf271c', 'hex'),
  bzip:Buffer.alloc(2,'425a', 'hex'),
  bzip2:Buffer.alloc(3,'425a68', 'hex'),
  bzip2File:Buffer.alloc(5,'425a683931', 'hex'),
  compress:Buffer.alloc(2,'1f9d', 'hex'), //.z
//  deflate:Buffer.alloc(1,'e', 'hex'),
  gzip:Buffer.alloc(2,'1f8b', 'hex'),
  isz:Buffer.alloc(4,'49735a21', 'hex'),  //Compressed ISO image
//  lz4:Buffer.alloc(1,'f', 'hex'),
  lz4Frame:Buffer.alloc(4,'04224d18', 'hex'), //  LZ4 Frame FormatRemark: LZ4 block format does not offer any magic bytes
  lzg:Buffer.alloc(3,'4c5a47', 'hex'),
  lzfse:Buffer.alloc(4,'62767832', 'hex'), //	bvx2 - Lempel-Ziv style data compression algorithm using Finite State Entropy coding. OSS by Apple
  lzh:Buffer.alloc(2,'1fa0', 'hex'),
  lzhNoCompression:Buffer.alloc(5,'2d686c302d', 'hex'), // Lempel Ziv Huffman archive file Method 0 (No compression)
  lzh5:Buffer.alloc(5,'2d686c352d', 'hex'), // Lempel Ziv Huffman archive file Method 5 (8KiB sliding window)
  lzip:Buffer.alloc(4,'4c5a4950', 'hex'),
  lzma:Buffer.alloc(1,'5d', 'hex'),
  lzma2xz:Buffer.alloc(6,'fd377a585a00', 'hex'), //	xz tar.xz 	XZ compression utility using LZMA2 compression 
  lzmaQuickBMS:Buffer.alloc(1,'2c', 'hex'),
  pkzip:Buffer.alloc(4,'504b0304', 'hex'), //.zip
  pkzipEmpty:Buffer.alloc(4,'504b0506', 'hex'),
  pkzipSpanned:Buffer.alloc(4,'504b0708', 'hex'), // zip,aar,apk,docx,epub,ipa,jar,kmz,maff,msix,odp,ods,odt,pk3,pk4,pptx,usdz,vsdx,xlsx,xpi
  rncv1:Buffer.alloc(4,'524e4301', 'hex'),
  rncv2:Buffer.alloc(4,'524e4302', 'hex'), 
  snappy:Buffer.alloc(10,'ff060000734e61507059', 'hex'),
  zlibRare:Buffer.alloc(1,'58', 'hex'),
  zlib:Buffer.alloc(1,'78', 'hex'),
  zlibNoCompression:Buffer.alloc(2,'7801', 'hex'),
  zlibBestSpeed:Buffer.alloc(2,'785e', 'hex'),
  zlibDefault:Buffer.alloc(2,'789c', 'hex'),
  zlibBestCompression:Buffer.alloc(2,'78da', 'hex'),
  zlibNoCompressionPresetDictionary:Buffer.alloc(2,'7820', 'hex'),
  zlibBestSpeedPresetDictionary:Buffer.alloc(2,'787d', 'hex'),
  zlibDefaultPresetDictionary:Buffer.alloc(2,'78bb', 'hex'),
  zlibBestCompressionPresetDictionary:Buffer.alloc(2,'78f9', 'hex'),
  zst:Buffer.alloc(4,'28b52ffd', 'hex'),	//Zstandard compress
};
compressionTool.prototype.isSignature={
    gzip:(data)=>compressionTool.prototype.signature.gzip.compare(data,0,2,0,2)==0
};

module.exports = compressionTool;