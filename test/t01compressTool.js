//const assert=require('assert');
const compressionTool = require("../compressionTool");
const testData="abcdefghijklimnopqrstuvwxyz0123456789"

function testCompress(done,method){
	const compressor=new compressionTool()
	if(method) compressor[method]();
	compressor.compress(testData,
		(compressed)=>{
			compressor.decompress(compressed,(data)=>{
				if(data==testData) done()
				else done("compressed/decompressed output not equal to orginal")
			}
			,done)
		},
		done
	);
}

describe('Compress/decompress', function() {
	it('compress default', function(done){
		testCompress(done); 
	});
	it('zlib', function(done){
		testCompress(done,"setZlib"); 
	});
	it('zlib speed', function(done){
		testCompress(done,"setZlibSpeed"); 
	});
	it('zlib compression', function(done){
		testCompress(done,"setZlibCompression"); 
	});
	it('gzip', function(done){ 
		testCompress(done,"setGzip"); 
	});
	it('gzip speed', function(done){ 
		testCompress(done,"setGzipSpeed"); 
	});
	it('gzip compression', function(done){ 
		testCompress(done,"setGzipCompression"); 
	});
	it('flate', function(done){ 
		testCompress(done,"setFlate"); 
	});
	it('brotli', function(done){ 
		testCompress(done,"setBrotli"); 
	});
	it('snappy', function(done){ 
		testCompress(done,"setSnappy"); 
	});
	it('lzma', function(done){ 
		testCompress(done,"setLzma"); 
	});
	it('lzma compression', function(done){ 
		testCompress(done,"setLzmaCompression"); 
	});
});
describe('magic byte', function() {
	it('gzip scan', function(done){
		const compressor=new compressionTool()
		compressor.setGzip();
		compressor.compress(testData,
			(compressed)=>{
				const signatures=compressor.signatureScan(compressed)
				if(signatures.toString()=="gzip") done()
				else done("signature not found, result: "+signatures.toString()+ " for: " +compressed.toString('hex'))
			},
			done
		);
	});
	it('is gzip', function(done){
		const compressor=new compressionTool()
		compressor.setGzip();
		compressor.compress(testData,
			(compressed)=>{
				if(compressor.isSignature.gzip(compressed)) done()
				else done("signature not found, expecting "+compressor.signature.gzip.toString('hex')+" found "+compressed.toString('hex'))
			},
			done
		);
	});
});
