# CompressionTool

Simple the various compression technologies into a single management function for node.js and add some new generai capabilities.

## Usage

    const anError=(err)=>console.error("failed with "+err)
    const testData="abcdefghijklimnopqrstuvwxyz0123456789"
	const compressor=new compressionTool()
	compressor.setGzip();
	compressor.compress(testData,
		(compressed)=>{
			compressor.decompress(compressed,(data)=>{
				if(data==testData) console.log("working correctly") 
				else anError("compressed/decompressed output not equal to orginal")
			}
			,anError)
		},
		anError
	);



## Support
Raises issues on https://gitlab.com/peter.prib/compressionTool/-/issues


## License
GNU GENERAL PUBLIC LICENSE Version 3,
